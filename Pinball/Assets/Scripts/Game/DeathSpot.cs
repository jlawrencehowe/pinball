﻿using UnityEngine;
using System.Collections;

public class DeathSpot : MonoBehaviour {

	private GameManager gameManager;

	// Use this for initialization
	void Start () {
	
		gameManager = GameObject.Find ("GameManager").GetComponent<GameManager> ();

	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public void OnTriggerEnter2D(Collider2D coll){
		if (coll.gameObject.tag == "Ball") {

			gameManager.Death();
			//death effect

		}
	}
}
