﻿using UnityEngine;
using System.Collections;

public class test : MonoBehaviour {
	
	Rigidbody2D rb;
	public float speed;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D>();
		rb.AddRelativeForce(Vector3.up * speed);

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
