﻿using UnityEngine;
using System.Collections;

public class CircleBumper : MonoBehaviour {

	
	public float force;
	private ScoreManager scoreManager;
	public GameObject spores;

	// Use this for initialization
	void Start () {
		
		scoreManager = GameObject.Find("ScoreManager").GetComponent<ScoreManager>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnCollisionEnter2D(Collision2D coll){
		if(coll.gameObject.tag == "Ball"){
			Instantiate(spores, this.transform.position, this.transform.rotation);
			this.GetComponent<AudioSource>().Play();
			Vector2 knockBack = new Vector2((transform.position.x - coll.gameObject.transform.position.x) * force,                 
			            (transform.position.y - coll.gameObject.transform.position.y) * force);
			this.GetComponent<Animator>().SetTrigger("Bumped");
			coll.gameObject.GetComponent<Rigidbody2D>().velocity = knockBack;
			scoreManager.AddScore(5000);
			//coll.GetComponent<Rigidbody2D>().AddRelativeForce(this.transform.up * force);
		}
	}

}
