﻿using UnityEngine;
using System.Collections;

public class SideBumper : MonoBehaviour {

	public float force;
	private ScoreManager scoreManager;

	// Use this for initialization
	void Start () {
		
		scoreManager = GameObject.Find("ScoreManager").GetComponent<ScoreManager>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnTriggerEnter2D(Collider2D coll){
		if(coll.gameObject.tag == "Ball"){
			this.GetComponent<AudioSource>().Play();
			this.GetComponent<Animator>().SetTrigger("Bumped");
			coll.GetComponent<Rigidbody2D>().velocity = this.transform.up * force;
			scoreManager.AddScore(5000);
			//coll.GetComponent<Rigidbody2D>().AddRelativeForce(this.transform.up * force);
		}
	}
}
