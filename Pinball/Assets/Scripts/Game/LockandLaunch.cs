﻿using UnityEngine;
using System.Collections;

public class LockandLaunch : MonoBehaviour {

	
	public float force, gravityForce;
	public bool pull;
	public float launchTimer;
	public GameObject ball;
	private float cdTimer;
	private ScoreManager scoreManager;

	// Use this for initialization
	void Start () {
		ball = GameObject.Find ("Ball");
		scoreManager = GameObject.Find("ScoreManager").GetComponent<ScoreManager>();
	}
	
	// Update is called once per frame
	void Update () {
		if(cdTimer >= 0)
			cdTimer -= Time.deltaTime;
		if (pull) {
			ball.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, ball.transform.position.z);
			launchTimer -= Time.deltaTime;

			if(launchTimer <= 0){
				pull = false;
				ball.GetComponent<Rigidbody2D>().velocity = (force * this.transform.up);
				cdTimer = 2;
				scoreManager.AddScore(5000);
				this.GetComponent<AudioSource>().Play();
			}
		}

	}

	public void OnTriggerEnter2D(Collider2D coll){
		if(coll.gameObject.tag == "Ball" && cdTimer <= 0){

			pull = true;
			launchTimer = 1;
			cdTimer = 2;

		}
	}
}
