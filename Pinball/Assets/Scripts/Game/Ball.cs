﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Ball : MonoBehaviour {

	public float currentPosX, prevPosX;
	public GameManager gameManager;
	public List<GameObject> safetyNets;
	public float maxSpeed;
	public int height;
	private float maxSize;
	private AudioSource audioSource;
	public AudioClip waterBall, safe;

	// Use this for initialization
	void Start () {
		audioSource = this.GetComponent<AudioSource>();
		maxSize = 1.5f;
		height = 1;
		currentPosX = this.transform.position.x;
		prevPosX = currentPosX;
		gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
	}
	
	// Update is called once per frame
	void Update () {

		prevPosX = currentPosX;
		currentPosX = this.transform.position.x;

		if(height == 1){
			gameObject.layer = LayerMask.NameToLayer( "Bottom" );
			Vector3 tempScale = this.transform.localScale;
			tempScale.x -= Time.deltaTime;
			tempScale.y -= Time.deltaTime;
			if(tempScale.x < 1){
				tempScale.x = 1;
				tempScale.y = 1;
				
			}
			this.transform.localScale = tempScale;
		}
		else if(height == 2){
			gameObject.layer = LayerMask.NameToLayer( "Middle" );
			Vector3 tempScale = this.transform.localScale;
			tempScale.x += Time.deltaTime;
			tempScale.y += Time.deltaTime;
			if(tempScale.x > 1.25f){
				tempScale.x = 1.25f;
				tempScale.y = 1.25f;
				
			}
			this.transform.localScale = tempScale;
		}
		else{
			gameObject.layer = LayerMask.NameToLayer( "Top" );
			Vector3 tempScale = this.transform.localScale;
			tempScale.x += Time.deltaTime;
			tempScale.y += Time.deltaTime;
			if(tempScale.x > maxSize){
				tempScale.x = maxSize;
				tempScale.y = maxSize;

			}
			this.transform.localScale = tempScale;
		}
	
	}

	public void FixedUpdate(){

		if(GetComponent<Rigidbody2D>().velocity.magnitude > maxSpeed)
		{
			GetComponent<Rigidbody2D>().velocity = GetComponent<Rigidbody2D>().velocity.normalized * maxSpeed;
		}

	}


	public void OnTriggerEnter2D(Collider2D coll){
		if(coll.gameObject.tag == "DoubleBall"){

			//GameObject temp = Instantiate(this.gameObject, this.transform.position, this.transform.rotation) as GameObject;
			Destroy(coll.gameObject);

		}
		if(coll.gameObject.tag == "SafetyBall"){
			for(int i = 0; i < safetyNets.Count; i++){
				safetyNets[i].SetActive(true);
				Destroy(coll.gameObject);

			}
			audioSource.PlayOneShot(waterBall);
		}
		if(coll.gameObject.tag == "SafetyNet"){
			for(int i = 0; i < safetyNets.Count; i++){
				safetyNets[i].SetActive(false);

				
			}
			audioSource.PlayOneShot(safe);
			gameManager.RespawnBall();
		}
	}
}
