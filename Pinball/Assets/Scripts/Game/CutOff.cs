﻿using UnityEngine;
using System.Collections;

public class CutOff : MonoBehaviour {

	public bool isCutOff;
	public float lerpTimer;
	public Vector3  startLoc;
	public Transform finalLoc;

	// Use this for initialization
	void Start () {
		isCutOff = false;
		lerpTimer = 0;
		startLoc = this.transform.position;
	}
	
	// Update is called once per frame
	void Update () {

		if(isCutOff){
			if(lerpTimer < 1)
				lerpTimer += Time.deltaTime;
			this.transform.position = Vector3.Lerp(startLoc, finalLoc.position, lerpTimer);

		}
		else{
			if(lerpTimer > 0)
				lerpTimer -= Time.deltaTime;
			this.transform.position = Vector3.Lerp(finalLoc.position, startLoc, 1 - lerpTimer);
		}
		Vector3 tempVec = this.transform.position;
		tempVec.z = 4.5f;
		this.transform.position = tempVec;
	
	}

	public void OnTriggerEnter2D(Collider2D coll){

		if(coll.gameObject.tag == "Ball"){
			isCutOff = true;
			this.GetComponent<AudioSource>().Play();
		}

	}


}
