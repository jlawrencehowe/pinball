﻿using UnityEngine;
using System.Collections;

public class PersistingLevelData : MonoBehaviour {

	
	public static PersistingLevelData levelData;
	public float score;
	public int lives;


	// Use this for initialization
	void Start () {

		if(levelData == null){
			levelData = this;
			DontDestroyOnLoad(gameObject);
		}
		else if (levelData != this){
			
			Destroy(gameObject);
			
		}
		

		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
