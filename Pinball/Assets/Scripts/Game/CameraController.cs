﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	public float lerpTimer;

	private Vector3 startPos, newLoc, prevLoc;
	public float shakeDecay = 0.05f;
	public float shakeIntensity;
	public float shakeSpeed;
	private float distance;
	public bool shaking = false;

	// Use this for initialization
	void Start () {
		distance = 0;
		startPos = this.transform.position;
		prevLoc = startPos;
		newLoc = startPos;
	}
	
	// Update is called once per frame
	void Update () {
	/*
		if(lerpTimer < 1){
			lerpTimer += Time.deltaTime * 10;
			if(lerpTimer > 1){
				lerpTimer = 1;
			}
			if(currentLevel == 2){
				this.transform.position = Vector3.Lerp(level1Loc, level2Loc, lerpTimer);

			}
			else{
				this.transform.position = Vector3.Lerp(level2Loc, level3Loc, lerpTimer);
			}
		}
*/
		if(shaking){
			shakeIntensity -= (Time.deltaTime * shakeDecay);
			distance += Time.deltaTime * shakeSpeed;
			this.transform.position = Vector2.Lerp(prevLoc, newLoc, distance);
			if(distance >= 1){
				distance = 0;
				NewLocation();
			}
			if(shakeIntensity <= 0){
				shakeIntensity = 0;
				shaking = false;
				distance = 0;
				prevLoc = this.transform.position;
			}
		}
		else{
			this.transform.position = Vector3.Lerp(prevLoc, startPos, distance);
			distance += Time.deltaTime * shakeSpeed;
		}
		
		this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, -10);


	}



	public void ShakeCamera(){
		shakeIntensity += 1f;
		if(shakeIntensity > 2f){
			shakeIntensity = 2f;
		}
		shaking = true;
	}

	private void NewLocation(){
		prevLoc = newLoc;
		newLoc.x = Random.Range(startPos.x - shakeIntensity, startPos.x + shakeIntensity);
	}
}
