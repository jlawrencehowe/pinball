﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class NextLevelStart : MonoBehaviour {

	public float force;
	private CameraController cameraController;
	public int level;
	public List<GameObject> deathSpotList;
	private GameManager gameManager;
	public Image fadeToBlack;
	public bool isFading;

	// Use this for initialization
	void Start () {
		cameraController = GameObject.Find("Main Camera").GetComponent<CameraController>();
		gameManager = GameObject.Find ("GameManager").GetComponent<GameManager> ();
	}
	
	// Update is called once per frame
	void Update () {
		if(isFading){
			fadeToBlack.color = new Color(0, 0, 0, fadeToBlack.color.a + Time.deltaTime);	
			if(fadeToBlack.color.a >= 1){
				GameObject.Find("MusicPlayer").GetComponent<MusicPlayer>().updateMusicClip(level + 1);
				Application.LoadLevel(level);
			}
		}
		else if(fadeToBlack.color.a > 0){
			fadeToBlack.color = new Color(0, 0, 0, fadeToBlack.color.a - Time.deltaTime);	
		}
	
	
	
	}

	public void OnTriggerExit2D(Collider2D coll){
		if(coll.gameObject.tag == "Ball"){
			this.GetComponent<BoxCollider2D>().enabled = false;
			isFading = true;
			gameManager.currentSection++;
			gameManager.RespawnBall();
			coll.gameObject.SetActive(false);
		}
	}
}
