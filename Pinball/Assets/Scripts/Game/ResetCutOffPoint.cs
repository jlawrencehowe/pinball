﻿using UnityEngine;
using System.Collections;

public class ResetCutOffPoint : MonoBehaviour {

	public CutOff cutOff;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnTriggerEnter2D(Collider2D coll){

		if(coll.gameObject.tag == "Ball"){
			Debug.Log(Application.persistentDataPath);
			cutOff.isCutOff = false;
		}

	}
}
