﻿using UnityEngine;
using System.Collections;

public class GravitySpot : MonoBehaviour {

	public float force, gravityForce;
	public bool startLaunch = false;
	public float launchTimer;
	private GameObject ball;
	private ScoreManager scoreManager;
	public GameObject actualSprite;
	public Animator animator;
	// Use this for initialization
	void Start () {

		ball = GameObject.Find ("Ball");
		scoreManager = GameObject.Find("ScoreManager").GetComponent<ScoreManager>();

	}
	
	// Update is called once per frame
	void Update () {
		launchTimer -= Time.deltaTime;
		if (launchTimer <= 0 && startLaunch) {
			animator.SetBool("StartLaunch", true);
			ball.gameObject.GetComponent<Rigidbody2D>().gravityScale = 1;
			Vector2 knockBack = new Vector2((transform.position.x - ball.transform.position.x) * force,                 
			                                  (transform.position.y - ball.transform.position.y) * force);
			ball.GetComponent<Rigidbody2D>().AddForce(knockBack);
			startLaunch = false;
			scoreManager.AddScore(10000);
			
			this.GetComponent<AudioSource>().Play();
		}
		else{
		
			animator.SetBool("StartLaunch", false);
		}

	}

	public void OnCollisionEnter2D(Collision2D coll){
		if(coll.gameObject.tag == "Ball"&& !startLaunch){
			ball.gameObject.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
			ball.gameObject.GetComponent<Rigidbody2D>().gravityScale = 0;
			startLaunch = true;
			launchTimer = 1;
			//coll.GetComponent<Rigidbody2D>().AddRelativeForce(this.transform.up * force);
		}
	}

	public void OnTriggerStay2D(Collider2D coll){
		if(coll.gameObject.tag == "Ball"){
			Vector2 gravityPull = new Vector2((transform.position.x - coll.gameObject.transform.position.x) * gravityForce,                 
			                                  (transform.position.y - coll.gameObject.transform.position.y) * gravityForce);
			coll.gameObject.GetComponent<Rigidbody2D>().AddForce(gravityPull);
			//coll.GetComponent<Rigidbody2D>().AddRelativeForce(this.transform.up * force);
		}
	}
}
