﻿using UnityEngine;
using System.Collections;

public class Portal : MonoBehaviour {

	private bool isDeactivated = false;
	public Portal otherPortal;
	public float deactivatedTimer;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		deactivatedTimer -= Time.deltaTime;
		if(deactivatedTimer <= 0){
			isDeactivated = false;
			this.GetComponent<Animator>().SetBool("Used", false);
		}
		else{
			isDeactivated = true;
			this.GetComponent<Animator>().SetBool("Used", true);
		}
		Vector3 tempRot = this.transform.eulerAngles;
		tempRot.z += Time.deltaTime * 10;
		this.transform.eulerAngles = tempRot;
	
	}

	public void OnTriggerEnter2D(Collider2D coll){
		if(coll.gameObject.tag == "Ball" && !isDeactivated){
			if(otherPortal != null){
				otherPortal.isDeactivated = true;
				otherPortal.deactivatedTimer = 1.5f;
				coll.gameObject.transform.position = otherPortal.gameObject.transform.position;
			
				deactivatedTimer = 1.5f;
				this.GetComponent<AudioSource>().Play();
			}
		}
	}

	public void OnTriggerExit2D(Collider2D coll){
		if(coll.gameObject.tag == "Ball"){
			isDeactivated = false;
		}
	}

}
