﻿using UnityEngine;
using System.Collections;

public class Fireworks : MonoBehaviour {

	private float explosionTimer = 1;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		explosionTimer -= Time.deltaTime;
		if(explosionTimer <= 0){
			this.GetComponent<Animator>().SetTrigger("Fire");
		}
		else{
			Vector3 tempPos = this.transform.position;
			tempPos.y += Time.deltaTime * 20;
			this.transform.position = tempPos;
		}

	}
}
