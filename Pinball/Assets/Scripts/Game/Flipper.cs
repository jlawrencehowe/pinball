﻿using UnityEngine;
using System.Collections;

public class Flipper : MonoBehaviour
{

	public float maxRightRotation, baseRightRotation, maxLeftRotation, baseLeftRotation;
	public bool rightFlipDown, leftFlipDown;
	public GameObject flipper;
	public float flipSpeed;
	public bool isRightFlipper;

	// Use this for initialization
	void Start ()
	{
	


	}
	
	// Update is called once per frame
	void Update ()
	{
	
		if (Input.GetButtonDown ("J")) {
			rightFlipDown = true;
			this.GetComponent<AudioSource>().Play();
		}
		if (Input.GetButtonUp ("J")) {
			rightFlipDown = false;
		}
		if (Input.GetButtonDown ("K")) {
			leftFlipDown = true;

			this.GetComponent<AudioSource>().Play();
		}
		if (Input.GetButtonUp ("K")) {
			leftFlipDown = false;
		}

		if (isRightFlipper) {
			if (rightFlipDown) {
				if(flipper.GetComponent<Transform> ().eulerAngles.z != maxRightRotation){
				//Vector3 flipRot = flipper.GetComponent<Transform> ().eulerAngles;
				Vector3 flipRot = flipper.GetComponent<Transform> ().eulerAngles;
				flipRot.z += Time.deltaTime * flipSpeed;
				flipper.GetComponent<Rigidbody2D> ().MoveRotation(flipRot.z);

				if (flipper.GetComponent<Transform> ().eulerAngles.z > maxRightRotation 
				    && flipper.GetComponent<Transform> ().eulerAngles.z < 180) {
					flipRot.z = 0;
					Vector3 tempRot = flipper.GetComponent<Transform> ().eulerAngles;
					tempRot.z = maxRightRotation;
					flipper.GetComponent<Transform> ().eulerAngles= tempRot;
				}
				}

			
			
				//flipper.GetComponent<Transform> ().Rotate(flipRot);
			} else {
				//Vector3 flipRot = flipper.GetComponent<Transform> ().eulerAngles;

				Vector3 flipRot = flipper.GetComponent<Transform> ().eulerAngles;
				flipRot.z -= Time.deltaTime * flipSpeed;
				flipper.GetComponent<Rigidbody2D> ().MoveRotation(flipRot.z);
				//flipper.GetComponent<Rigidbody2D> ().MoveRotation(flipRot.z);
				if (flipper.GetComponent<Transform> ().eulerAngles.z <= baseRightRotation 
				    && flipper.GetComponent<Transform> ().eulerAngles.z > 180) {
					flipRot.z = 0;
					Vector3 tempRot = flipper.GetComponent<Transform> ().eulerAngles;
					tempRot.z = baseRightRotation;
					flipper.GetComponent<Transform> ().eulerAngles = tempRot;
				}

				//flipper.GetComponent<Transform> ().Rotate(flipRot);
			}
		} else {
			if (leftFlipDown) {
				if(flipper.GetComponent<Transform> ().eulerAngles.z != maxLeftRotation){
				
				//Vector3 flipRot = flipper.GetComponent<Transform> ().eulerAngles;
				Vector3 flipRot = flipper.GetComponent<Transform> ().eulerAngles;
				flipRot.z -= Time.deltaTime * flipSpeed;
				flipper.GetComponent<Rigidbody2D> ().MoveRotation(flipRot.z);
				
				if (flipper.GetComponent<Transform> ().eulerAngles.z < maxLeftRotation
				    && flipper.GetComponent<Transform> ().eulerAngles.z < 180) {
					
					flipRot.z = 0;
					Vector3 tempRot = flipper.GetComponent<Transform> ().eulerAngles;
					tempRot.z = maxLeftRotation;
					flipper.GetComponent<Transform> ().eulerAngles = tempRot;
				}
				}
			
				//flipper.GetComponent<Transform> ().Rotate(flipRot);
			} else {

				//Vector3 flipRot = flipper.GetComponent<Transform> ().eulerAngles;
				Vector3 flipRot = flipper.GetComponent<Transform> ().eulerAngles;
				flipRot.z += Time.deltaTime * flipSpeed;
				flipper.GetComponent<Rigidbody2D> ().MoveRotation(flipRot.z);
				
				if (flipper.GetComponent<Transform> ().eulerAngles.z >= baseLeftRotation
				    && flipper.GetComponent<Transform> ().eulerAngles.z > 180) {

					flipRot.z = 0;
					Vector3 tempRot = flipper.GetComponent<Transform> ().eulerAngles;
					tempRot.z = baseLeftRotation;
					flipper.GetComponent<Transform> ().eulerAngles = tempRot;
				}

				//flipper.GetComponent<Transform> ().Rotate(flipRot);
			}
		}

	}
}
