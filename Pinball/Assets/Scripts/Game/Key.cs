﻿using UnityEngine;
using System.Collections;

public class Key : MonoBehaviour {

	public bool captured = false;
	public GameManager gameManager;
	public ScoreManager scoreManager;

	// Use this for initialization
	void Start () {
		gameManager = GameObject.Find ("GameManager").GetComponent<GameManager> ();
		scoreManager = GameObject.Find("ScoreManager").GetComponent<ScoreManager>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnTriggerEnter2D(Collider2D coll){

		if(coll.gameObject.tag == "Ball" && !captured){
			captured = true;
			//light up
			this.GetComponent<AudioSource>().Play();
			gameManager.AddKeys();
			scoreManager.AddScore(10000);
			this.GetComponent<Animator>().SetTrigger("Lit");
		}

	}
}
