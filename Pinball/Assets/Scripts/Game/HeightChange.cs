﻿using UnityEngine;
using System.Collections;

public class HeightChange : MonoBehaviour {

	public int height;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnTriggerEnter2D(Collider2D coll){

		if(coll.gameObject.tag == "Ball"){
			coll.gameObject.GetComponent<Ball>().height = height;

		}

	}
}
