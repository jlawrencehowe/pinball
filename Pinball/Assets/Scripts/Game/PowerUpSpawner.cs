﻿using UnityEngine;
using System.Collections;

public class PowerUpSpawner : MonoBehaviour {

	public float timer = 120;
	public GameObject safetyBall;
	public GameObject currentSafetyBall;
	public GameObject activeSafetyNet;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {

		if(currentSafetyBall == null && !activeSafetyNet.activeInHierarchy)
			timer -= Time.deltaTime;
		if(timer <= 0){
			this.GetComponent<AudioSource>().Play();
			currentSafetyBall = Instantiate(safetyBall, this.transform.position, this.transform.rotation) as GameObject;
			timer = 120;
		}
	
	}
}
