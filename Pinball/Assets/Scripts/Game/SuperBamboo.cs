﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SuperBamboo : MonoBehaviour {

	public Animator superAnimator, key1, key2, key3, scoreKey1, scoreKey2, scoreKey3;
	public bool ready = false;
	public GameManager gameManager;
	public ScoreManager scoreManager;
	public bool spawnFireworks;
	public float fireWorksTimer = 0;
	public GameObject blueFireWork, redFireWork, yellowFireWork;
	public int currFirework = 0;
	public List<Transform> fireWorkLocs;

	// Use this for initialization
	void Start () {
		gameManager = GameObject.Find ("GameManager").GetComponent<GameManager> ();
		scoreManager = GameObject.Find("ScoreManager").GetComponent<ScoreManager>();
	}
	
	// Update is called once per frame
	void Update () {

		if(spawnFireworks){
			fireWorksTimer -= Time.deltaTime;
			if(fireWorksTimer <= 0){
				int temp = Random.Range(0, 2);
				if(temp == 0)
					Instantiate(blueFireWork,fireWorkLocs[currFirework].position, this.transform.rotation);
				else if(temp == 1)
					Instantiate(redFireWork,fireWorkLocs[currFirework].position, this.transform.rotation);
				else
					Instantiate(yellowFireWork,fireWorkLocs[currFirework].position, this.transform.rotation);
				fireWorksTimer = 0.5f;
				currFirework++;
				if(currFirework >= 10){
					spawnFireworks = false;
				}
			}
		}
	
	}

	public void OnTriggerEnter2D(Collider2D coll){

		if(coll.gameObject.tag == "Ball" && ready){
			ready = false;
			//light up
			spawnFireworks = true;
			this.GetComponent<AudioSource>().Play();
			key1.GetComponent<Key>().captured = false;
			key2.GetComponent<Key>().captured = false;
			key3.GetComponent<Key>().captured = false;
			key1.SetTrigger("Idle");
			key2.SetTrigger("Idle");
			key3.SetTrigger("Idle");
			scoreKey1.SetTrigger("Lit");
			scoreKey2.SetTrigger("Lit");
			scoreKey3.SetTrigger("Lit");
			scoreManager.AddScore(100000);
			this.GetComponent<Animator>().SetTrigger("Unlit");
		}

	}
}
