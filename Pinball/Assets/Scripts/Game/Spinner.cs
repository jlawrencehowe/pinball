﻿using UnityEngine;
using System.Collections;

public class Spinner : MonoBehaviour {

	private ScoreManager scoreManager;
	public float bottomMaxRotation, topMaxRotation;
	public GameObject ball;
	public double ballPrevX, ballCurrX;
	public bool isRotating;
	public Vector3 ballLoc;
	public float flipSpeed;
	public float ballVelocity;


	// Use this for initialization
	void Start () {

		scoreManager = GameObject.Find("ScoreManager").GetComponent<ScoreManager>();
		ballCurrX = 0;
		ballPrevX = 0;
	}
	
	// Update is called once per frame
	void Update () {

	

		if (isRotating) {

			if (ballCurrX >= ballPrevX) {
				Debug.Log(1);
				Vector3 flipRot = this.transform.eulerAngles;
				flipRot.z += Time.deltaTime * flipSpeed;
				this.GetComponent<Rigidbody2D> ().MoveRotation (flipRot.z);
				
				if (this.GetComponent<Transform> ().eulerAngles.z >= bottomMaxRotation 
					&& this.GetComponent<Transform> ().eulerAngles.z < 180) {
					flipRot.z = 0;
					Vector3 tempRot = this.GetComponent<Transform> ().eulerAngles;
					tempRot.z = bottomMaxRotation;
					this.GetComponent<Transform> ().eulerAngles = tempRot;
					isRotating = false;
				}

			} else if (ballCurrX <  ballPrevX) {
				Debug.Log(2);
				Vector3 flipRot = this.transform.eulerAngles;
				flipRot.z -= Time.deltaTime * flipSpeed;
				this.GetComponent<Rigidbody2D> ().MoveRotation (flipRot.z);
					
				if (this.GetComponent<Transform> ().eulerAngles.z <= topMaxRotation 
					&& this.GetComponent<Transform> ().eulerAngles.z > 180) {
					flipRot.z = 0;
					Vector3 tempRot = this.GetComponent<Transform> ().eulerAngles;
					tempRot.z = topMaxRotation;
					this.GetComponent<Transform> ().eulerAngles = tempRot;
					isRotating = false;
				}


			}
		} else {
			if(ballCurrX < ballPrevX){
				Debug.Log(3);
				Vector3 flipRot = this.transform.eulerAngles;
				flipRot.z += Time.deltaTime * flipSpeed;
				this.GetComponent<Rigidbody2D> ().MoveRotation (flipRot.z);
				
				if ((this.GetComponent<Transform> ().eulerAngles.z < 90 && this.GetComponent<Transform> ().eulerAngles.z > 0) 
				    || (this.GetComponent<Transform> ().eulerAngles.z < 359.9f && this.GetComponent<Transform> ().eulerAngles.z > 300)) {
					flipRot.z = 0;
					Vector3 tempRot = this.GetComponent<Transform> ().eulerAngles;
					tempRot.z = 0;
					this.GetComponent<Transform> ().eulerAngles = tempRot;

				}

			}
			else if(ballCurrX > ballPrevX){
				Debug.Log(4);
				Vector3 flipRot = this.transform.eulerAngles;
				flipRot.z -= Time.deltaTime * flipSpeed;
				this.GetComponent<Rigidbody2D> ().MoveRotation (flipRot.z);
				
				if (this.GetComponent<Transform> ().eulerAngles.z < 359.9f && this.GetComponent<Transform> ().eulerAngles.z > 300 
				    || (this.GetComponent<Transform> ().eulerAngles.z < 90 && this.GetComponent<Transform> ().eulerAngles.z > 0)) {
					flipRot.z = 0;
					Vector3 tempRot = this.GetComponent<Transform> ().eulerAngles;
					tempRot.z = 0;
					this.GetComponent<Transform> ().eulerAngles = tempRot;
					
				}
			}
			else{
				Vector3 tempRot = this.GetComponent<Transform> ().eulerAngles;
				tempRot.z = 0;
				this.GetComponent<Transform> ().eulerAngles = tempRot;
			}
		}

	}

	public void OnTriggerEnter2D(Collider2D coll){
		if(coll.gameObject.tag == "Ball" && !isRotating){
			this.GetComponent<AudioSource>().Play();
			scoreManager.AddScore(7500);
			isRotating = true;
			ballLoc = coll.gameObject.transform.position;
			ballVelocity = coll.gameObject.GetComponent<Rigidbody2D>().velocity.x;
			ballCurrX = coll.gameObject.GetComponent<Ball>().currentPosX;
			ballPrevX = coll.gameObject.GetComponent<Ball>().prevPosX;

		}
	}
}
