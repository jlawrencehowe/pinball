﻿using UnityEngine;
using System.Collections;

public class RollOverPoints : MonoBehaviour {


	private ScoreManager scoreManager;

	// Use this for initialization
	void Start () {
		scoreManager = GameObject.Find("ScoreManager").GetComponent<ScoreManager>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public void OnTriggerEnter2D(Collider2D coll){

		if(coll.gameObject.tag == "Ball"){
			scoreManager.AddScore(5000);
			this.GetComponent<Animator>().SetTrigger("Rolled");
			this.GetComponent<AudioSource>().Play();
			//light up
		}

	}

	public void OnTriggerExit2D(Collider2D coll){
		if(coll.gameObject.tag == "Ball"){
			//light off
		}
	}
}
