﻿using UnityEngine;
using System.Collections;

public class Launcher : MonoBehaviour {

	public float force;
	public GameObject ball;
	public bool ballReady = true;

	// Use this for initialization
	void Start () {
		ball = GameObject.Find ("Ball");
	}
	
	// Update is called once per frame
	void Update () {
	
		if (Input.GetButton ("1")) {
			Vector3 tempScale = this.transform.localScale;
			if (this.transform.localScale.y > 0.25f) {
				tempScale.y -= Time.deltaTime;
				if (tempScale.y <= 0.25f) {
					tempScale.y = 0.25f;
				}
				this.transform.localScale = tempScale;
			}
		} else {
			Vector3 tempScale = this.transform.localScale;
			if (this.transform.localScale.y < 0.65f) {
				tempScale.y += Time.deltaTime * 3;
				if (tempScale.y >= 0.65f) {
					tempScale.y = 0.65f;
				}
				this.transform.localScale = tempScale;
			}
		}

		if(Input.GetButtonUp("1") && ballReady){
			ball.GetComponent<Rigidbody2D>().velocity = force * (1 - this.transform.localScale.y) * Vector3.up;
			this.GetComponent<AudioSource>().Play();
		}

	}

	public void OnTriggerEnter2D(Collider2D coll){
		if (coll.gameObject.tag == "Ball") {
			ballReady = true;
		}
	}
	public void OnTriggerExit2D(Collider2D coll){
		if (coll.gameObject.tag == "Ball") {
			ballReady = false;
		}
	}


}
