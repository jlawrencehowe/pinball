﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

	public int lives = 3;
	public GameObject deathSpotList;
	public int currentSection = 1;
	public int keys = 0;
	public int tiltCharge;
	public List<GameObject> keyList;
	public int maxKeys;
	private GameObject ball;
	public float tiltForce, tiltChargeTimer;
	public GameObject spawnPoints;
	public GameObject doors;
	public GameObject endPoints;
	public GameObject gates;
	public List<SpriteRenderer> livesSprite;
	public bool openNextLevel;
	public float doorLerp;
	public Canvas highScoreCanvas;
	public Text startOverText;
	public ScoreManager scoreManager;
	public List<Text> highScoreText, namesText;
	public PersistingGameData persistingGD;
	private bool waitPlayer = false;
	public CameraController cameraController;
	public PersistingLevelData persistingLevelData;
	public CutOff cutOff;
	public bool newHighScore = false;
	public int highScoreSpot = -1, charSpot = 1;
	public string userName;
	public char currentChar, char1, char2, char3;
	public Text inputNameText;
	public Text textChar1, textChar2, textChar3;
	public int inGameHighScorePos = 11;
	public GameObject nextLevel;

	public AudioSource audioSource;
	public AudioClip death, gameOver;
	public SuperBamboo superBamboo;

	public Image inputNameBackground, border;
	public Text askInputNameText;


	// Use this for initialization
	void Start () {
		currentChar = 'A';
		inputNameBackground.enabled = false;
		askInputNameText.enabled = false;
		border.enabled = false;
		textChar1.enabled = false;
		textChar2.enabled = false;
		textChar3.enabled = false;
		startOverText.enabled = false;
		persistingLevelData = GameObject.Find("PersistingLevelData").GetComponent<PersistingLevelData>();
		lives = persistingLevelData.lives;
		//highScoreCanvas.enabled = false;
		ball = GameObject.Find ("Ball");
		for(int i = lives; i < 3; i++){
			livesSprite[lives].color = new Color32(81, 81, 81, 255);
		}
		for(int i = 0; i < maxKeys; i++){
			keyList[i].GetComponent<Animator>().SetTrigger("Lit");
		}

	}
	
	// Update is called once per frame
	void Update () {
		/*
		if (tiltCharge < 3) {
			tiltChargeTimer -= Time.deltaTime;
			if(tiltChargeTimer <= 0){
				tiltCharge++;
				tiltChargeTimer = 5;
			}
		}
		*/
		UpdateMidGameScore();
		UpdateScoreTable();
		if(newHighScore){
			inputNameBackground.enabled = true;
			askInputNameText.enabled = true;
			border.enabled = true;
			textChar1.enabled = true;
			textChar2.enabled = true;
			textChar3.enabled = true;
			if(charSpot == 1)
				textChar1.text = currentChar.ToString();
			if(charSpot == 2){
				textChar2.text = currentChar.ToString();
			}
			if(charSpot == 3){
				textChar3.text = currentChar.ToString();
			}
			inputNameText.enabled = true;
			if(Input.GetButtonDown("J")){
				if(currentChar > 65)
					currentChar--;
			}
			if(Input.GetButtonDown("K")){
				if(currentChar < 90)
				currentChar++;
			}
			if(Input.GetButtonDown ("1")){
				if(charSpot == 1){
					charSpot++;
					char1 = currentChar;
				}
				else if(charSpot == 2){
					charSpot++;
					char2 = currentChar;
				}
				else{
					char3 = currentChar;
					userName = char1.ToString() + char2.ToString() + char3.ToString();
					persistingGD.names[highScoreSpot] = userName;
					startOverText.enabled = true;
					//UpdateHighScore();
					persistingGD.saveGameInfoData();
					newHighScore = false;
				}

			}
		}
		else if (waitPlayer && Input.GetButtonDown ("1")) {
			persistingLevelData.score = 0;
			persistingLevelData.lives = 3;
			GameObject.Find("MusicPlayer").GetComponent<MusicPlayer>().updateMusicClip(2);
			Application.LoadLevel(1);
		}
		if(waitPlayer && !newHighScore){
			startOverText.enabled = true;
		}
		if(Input.GetButtonDown("3")){
			Application.Quit();
		}
		if (Input.GetButtonDown ("U")  && !cameraController.shaking) {
			cameraController.ShakeCamera();
			ball.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.right * tiltForce);
			tiltCharge--;
		}
		if (Input.GetButtonDown ("I")  && !cameraController.shaking) {
			cameraController.ShakeCamera();
			ball.GetComponent<Rigidbody2D>().AddRelativeForce(-Vector2.right * tiltForce);
			tiltCharge--;
		}

		if(openNextLevel){
			doorLerp += Time.deltaTime;
			doors.transform.position = Vector3.Lerp(gates.transform.position,endPoints.transform.position,doorLerp);

		}


	}

	public void Death(){
		lives--;
		livesSprite[lives].color = new Color32(81, 81, 81, 255);
		if (lives <= 0) {
			audioSource.PlayOneShot(gameOver);
			scoreManager.currentScore = scoreManager.actualScore;
			UpdateHighScore();
			highScoreCanvas.enabled = true;
			persistingGD.saveGameInfoData ();
			waitPlayer = true;


		} else {
			audioSource.PlayOneShot(death);
			ball.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
			ball.transform.position = spawnPoints.transform.position;
			Vector3 tempPos = ball.transform.position;
			tempPos.z = -1;
			ball.transform.position = tempPos;
		}
		persistingLevelData.lives = lives;
		cutOff.isCutOff = false;
	}

	public void RespawnBall(){
		ball.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
		ball.transform.position = spawnPoints.transform.position;
		Vector3 tempPos = ball.transform.position;
		tempPos.z = -1;
		ball.transform.position = tempPos;
		cutOff.isCutOff = false;
	}

	public void AddKeys(){
		keys++;
		keyList[keys - 1].GetComponent<Animator>().SetTrigger("Idle");


		//update lights
		if (keys >= maxKeys && currentSection != 3) {

			openNextLevel = true;
			nextLevel.GetComponent<BoxCollider2D>().enabled = true;
			doorLerp = 0;


		}
		else if(keys >= maxKeys && currentSection == 3){
			superBamboo.ready = true;
			superBamboo.GetComponent<Animator>().SetTrigger("Lit");
			keys = 0;
		}
	}

	public void ReactivateKeys(){
		
	}

	public void UpdateScoreTable(){
		for (int i = 0; i < 10; i++) {
			highScoreText [i].text = "" + persistingGD.highScores [i];
			namesText [i].text = persistingGD.names [i] + ":";
		}
	}

	private void UpdateMidGameScore(){


		if(inGameHighScorePos == 11){
			if(scoreManager.actualScore > persistingGD.highScores[9]){
				persistingGD.highScores[9] = (int)scoreManager.actualScore;
				persistingGD.names[9] = "New Score";
				inGameHighScorePos--;
			}
		}
		else if(inGameHighScorePos > 1){
			if(scoreManager.actualScore > persistingGD.highScores[inGameHighScorePos - 2]){

				persistingGD.highScores[inGameHighScorePos - 1] = persistingGD.highScores[inGameHighScorePos - 2];
				persistingGD.names[inGameHighScorePos - 1] =  persistingGD.names[inGameHighScorePos - 2];
				persistingGD.names[inGameHighScorePos - 2] = "New Score";
				persistingGD.highScores[inGameHighScorePos - 2] = (int)scoreManager.actualScore;
				inGameHighScorePos--;
			}
		}
		else{
			persistingGD.highScores[0] = (int)scoreManager.actualScore;
		}

		if(inGameHighScorePos <= 10)
			persistingGD.highScores[inGameHighScorePos - 1] = (int)scoreManager.actualScore;

	}

	private void UpdateHighScore ()
	{

		for (int i = 0; i < 10; i++) {

			if (scoreManager.actualScore == persistingGD.highScores [i]) {
				Debug.Log("Test");
				persistingGD.highScores.Insert (i, (int)scoreManager.currentScore);
				persistingGD.highScores.RemoveAt (i + 1);
				persistingGD.names.Insert (i, " ");
				persistingGD.names.RemoveAt (i + 1);
				if(lives <= 0)
					newHighScore = true;
				highScoreSpot = i;
				break;
			}
		}
		
		for (int i = 0; i < 10; i++) {
			highScoreText [i].text = "" + persistingGD.highScores [i];
			namesText [i].text = persistingGD.names [i] + ":";
		}
		
	}
}
