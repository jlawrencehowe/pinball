﻿using UnityEngine;
using System.Collections;

public class ScoreManager : MonoBehaviour {

	public float actualScore;
	public float currentScore;
	public TextMesh scoreText;
	private PersistingLevelData persistingLevelData;
	public GameManager gameManager;


	// Use this for initialization
	void Start () {

		gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
		persistingLevelData = GameObject.Find("PersistingLevelData").GetComponent<PersistingLevelData>();
		actualScore = persistingLevelData.score;
		currentScore = persistingLevelData.score;
		scoreText.text = "Score: " + string.Format ("{0:00000000}", currentScore);
	}
	
	// Update is called once per frame
	void Update () {
	
		if(currentScore < actualScore){
			currentScore += Time.deltaTime * 50000;
		}
		if(currentScore > actualScore){
			currentScore = actualScore;
		}
		scoreText.text = "Score: " + string.Format("{0:00000000}", currentScore);

	}

	public void AddScore(int points){
		if(gameManager.currentSection == 2)
			points = (int)(points * 1.5f);
		else if(gameManager.currentSection == 3){
			
			points = points * 2;
		}
		actualScore += points;
		persistingLevelData.score = actualScore;
	}
}
