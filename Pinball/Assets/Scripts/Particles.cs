﻿using UnityEngine;
using System.Collections;

public class Particles : MonoBehaviour {

	public AudioClip explosion;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void DestroyThis(){
		Destroy(gameObject);
	}

	public void PlayExplosion(){
		this.GetComponent<AudioSource>().PlayOneShot(explosion);
	}
}
