﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class MainMenu : MonoBehaviour {

	public GameObject controlsImage;

	// Use this for initialization
	void Start () {
		controlsImage.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
		if (Input.GetButtonDown ("1")) {
			if(!controlsImage.activeInHierarchy){
				controlsImage.SetActive(true);
				this.GetComponent<AudioSource>().Play();
			}
			else{
				GameObject.Find("MusicPlayer").GetComponent<MusicPlayer>().updateMusicClip(2);
				Application.LoadLevel(1);
			}
		}
		if(Input.GetButtonDown("3")){
			Application.Quit();
		}

	}
}
